#lang racket

; Copyright 2021 Pi Fisher (3geek14)
; 
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
; 
;     http://www.apache.org/licenses/LICENSE-2.0
; 
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.

(provide index-md)

(define (index-md folder-contents)
  (string-append (title-string)
                 (md-builder
                  (group-list->tree
                   (contents->contents-by-folder folder-contents))
                  0)))

(define (title-string)
  "# Index of All Files #\n\n")

(define (spaces indentation)
  (apply string-append (vector->list (make-vector indentation "  "))))

(define (md-builder tree indentation)
  (string-append (md-folder-name (first (first tree))
                                 indentation)
                 (md-folder-contents (first (first tree))
                                     (second (first tree))
                                     (+ 1 indentation))
                 (foldr string-append ""
                        (map (lambda (folder)
                               (md-builder folder (+ 1 indentation)))
                             (second tree)))))

(define (md-folder-name name indentation)
  (string-append (spaces indentation)
                 "* __Realms"
                 (substring name 6 (- (string-length name) 1))
                 "/:__\n"
                 (if (= indentation 0)
                     "  * [index.html](./index.html)\n"
                     "")))

(define (md-folder-contents folder files indentation)
  (if (empty? files)
      ""
      (string-append (spaces indentation)
                     "* ["
                     (first files)
                     "](."
                     (substring folder 6 (- (string-length folder) 1))
                     "/"
                     (first files)
                     ")\n"
                     (md-folder-contents folder (rest files) indentation))))

(define (contents->contents-by-folder contents)
  (filter (lambda (l) (not (empty? l)))
          (contents->contents-by-folder-helper contents empty)))

(define (contents->contents-by-folder-helper contents group-so-far)
  (if (empty? contents)
      (list (process-group group-so-far))
      (if (string-contains? (first contents) ":")
          (cons (process-group group-so-far)
                (contents->contents-by-folder-helper (rest contents)
                                                     (list (first contents))))
          (contents->contents-by-folder-helper (rest contents)
                                               (cons (first contents) group-so-far)))))

(define (is-file? content)
  (or (string-contains? content ".html")
      (string-contains? content ".css")))

(define (process-group group)
  (if (empty? group)
      empty
      (list (last group)
            (reverse (filter is-file? group))
            (rest (reverse (filter (compose not is-file?) group))))))

(define (group-list->tree group-list)
  (let ([root (find-root group-list)])
    (list root
          (find-children root group-list))))

(define (find-root group-list)
  (first (filter (lambda (g) (string=? "public:" (first g)))
                 group-list)))

(define (find-children root group-list)
  (let ([children (filter (lambda (g)
                            (not
                             (empty?
                              (filter (lambda (f)
                                        (string=?
                                         (first g)
                                         (string-append (substring
                                                         (first root) 0
                                                         (- (string-length
                                                             (first root))
                                                            1))
                                                        "/" f ":")))
                                      (third root)))))
                          group-list)])
    (map (lambda (child)
           (list child (find-children child group-list)))
         children)))

;(define sample-contents
;  (list "public:"
;        "EditingProcedures.html"
;        "MarkdownTutorial.html"
;        "Omnibus.html"
;        "README.html"
;        "branches"
;        "external.css"
;        "internal.css"
;        "tags"
;        "year"
;        "public/branches:"
;        "2019"
;        "2020"
;        "master"
;        "update-build"
;        "public/branches/2019:"
;        "EditingProcedures.html"
;        "MarkdownTutorial.html"
;        "Omnibus.html"
;        "README.html"
;        "external.css"
;        "internal.css"
;        "public/branches/2020:"
;        "EditingProcedures.html"
;        "MarkdownTutorial.html"
;        "Omnibus.html"
;        "README.html"
;        "external.css"
;        "internal.css"
;        "public/branches/master:"
;        "EditingProcedures.html"
;        "MarkdownTutorial.html"
;        "Omnibus.html"
;        "README.html"
;        "external.css"
;        "internal.css"
;        "public/branches/update-build:"
;        "EditingProcedures.html"
;        "MarkdownTutorial.html"
;        "Omnibus.html"
;        "README.html"
;        "external.css"
;        "internal.css"
;        "public/tags:"
;        "2018.0.0"
;        "2019.0.0"
;        "2019.0.1"
;        "public/tags/2018.0.0:"
;        "Omnibus.html"
;        "README.html"
;        "pandoc.css"
;        "public/tags/2019.0.0:"
;        "EditingProcedures.html"
;        "MarkdownTutorial.html"
;        "Omnibus.html"
;        "README.html"
;        "pandoc.css"
;        "public/tags/2019.0.1:"
;        "EditingProcedures.html"
;        "MarkdownTutorial.html"
;        "Omnibus.html"
;        "README.html"
;        "external.css"
;        "internal.css"
;        "public/year:"
;        "2018"
;        "2019"
;        "public/year/2018:"
;        "Omnibus.html"
;        "README.html"
;        "pandoc.css"
;        "public/year/2019:"
;        "EditingProcedures.html"
;        "MarkdownTutorial.html"
;        "Omnibus.html"
;        "README.html"
;        "external.css"
;        "internal.css"))

;(define list-of-groups (contents->contents-by-folder sample-contents))
;(define tree-of-groups (group-list->tree (contents->contents-by-folder sample-contents)))